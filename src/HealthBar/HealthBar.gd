tool
extends Node2D

export(int, 12) var active_points setget set_active_points, get_active_points
export(int, 10) var regular_points setget set_regular_points
export(int, 2) var bonus_points setget set_bonus_points

const HEALTH_POINT = preload("res://HealthBar/HealthPoint.tscn")
const BONUS_HEALTH_POINT = preload("res://HealthBar/BonusHealthPoint.tscn")

func set_active_points(new_active_points:int) -> void:
	active_points = new_active_points
	_activate_points()

func get_active_points() -> int:
	return int(min(
		active_points,
		bonus_points + regular_points
	))

func set_regular_points(new_regular_points:int) -> void:
	regular_points = new_regular_points
	_rebuild_points()
	_activate_points()

func set_bonus_points(new_bonus_points:int) -> void:
	bonus_points = new_bonus_points
	_rebuild_points()
	_activate_points()

func is_empty() -> bool:
	return (active_points <= 0)

func is_full() -> bool:
	var available_points = regular_points + bonus_points
	return (available_points <= active_points)

func _rebuild_points():
	# Remove all points
	for point in get_children():
		remove_child(point)
	
	# Add bonus points
	var offset = 2 + ((bonus_points + regular_points) * 3 + 1) / -2
	for _i in range(0, bonus_points):
		var point = BONUS_HEALTH_POINT.instance()
		add_child(point)
		point.position.x = offset
		offset += 3

	# Add regular points
	for _i in range(0, regular_points):
		var point = HEALTH_POINT.instance()
		add_child(point)
		point.position.x = offset
		offset += 3

func _activate_points():
	# Update active status of all points
	var active_countdown = active_points
	for point in get_children():
		point.active = (active_countdown > 0)
		active_countdown -= 1
