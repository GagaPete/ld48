tool
extends Node2D

export(bool) var active = false setget set_active

func _ready():
	set_active(active)

func set_active(new_active:bool) -> void:
	if not has_node("AnimatedSprite"):
		return

	active = new_active
	if active:
		$AnimatedSprite.play("active")
	else:
		$AnimatedSprite.play("inactive")
