extends Node2D

onready var slots = [
	$Slots/Slot1,
	$Slots/Slot2,
	$Slots/Slot3,
	$Slots/Slot4,
];
onready var stack = $Stack

func _process(_delta:float) -> void:
	var stack_size = stack.get_child_count()
	if stack_size == 0:
		return

	for slot in slots:
		if slot.get_child_count() == 0:
			var card = stack.get_child(rand_range(0, stack_size))
			stack.remove_child(card)
			slot.add_child(card)
			card.position = Vector2()
			card.visible = true

			stack_size -= 1
			if stack_size == 0:
				stack.visible = false
				return

# Called from TreasureChest and possibly Trader
func add_card(card:Card):
	card.visible = false
	stack.add_child(card)
	stack.visible = true
