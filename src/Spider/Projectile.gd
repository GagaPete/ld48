extends Node2D
class_name SpiderProjectile

export(bool) var flipped = false
export(float) var speed

var sender:Spider

func _process(delta:float):
	if flipped:
		position.x += speed * delta
	else:
		position.x -= speed * delta

func _on_Area2D_area_entered(warrior):
	warrior.attack(1, sender)
	queue_free()
