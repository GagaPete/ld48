tool extends Node2D
class_name Spider

export(bool) var flipped = false setget set_flipped

onready var sprite = $AnimatedSprite
onready var raycast_forward = $RaycastForward
onready var health_bar = $HealthBar
onready var warrior_target = $WarriorTarget

var current_state = null

func _ready():
	if not Engine.editor_hint:
		current_state = $States/Idling
		current_state.enter()

func _process(delta:float):
	if current_state:
		current_state.process(delta)

func change_state(state_name:String) -> void:
	if not current_state:
		return

	current_state.leave()
	var new_state = $States.get_node(state_name)
	if new_state:
		new_state.enter()
	current_state = new_state

func _on_WarriorTarget_attacked(damage:float, _attacker) -> void:
	health_bar.active_points -= damage
	if health_bar.is_empty():
		change_state("Dying")

func set_flipped(new_flipped:bool) -> void:
	if not has_node("AnimatedSprite"):
		return
	flipped = new_flipped
	if flipped:
		$AnimatedSprite.scale.x = -1
		$WarriorTarget.position.x = -7
		$RaycastForward.position.x = 16
		$RaycastForward.cast_to.x = 100
	else:
		$AnimatedSprite.scale.x = 1
		$WarriorTarget.position.x = 8
		$RaycastForward.position.x = -16
		$RaycastForward.cast_to.x = -100
