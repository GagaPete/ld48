extends SpiderState

const PROJECTILE = preload("res://Spider/Projectile.tscn")
onready var timer = $AttackTimer

func enter() -> void:
	shoot()
	timer.start()

func leave() -> void:
	timer.stop()

func process(_delta:float) -> void:
	if not spider.raycast_forward.is_colliding():
		spider.change_state("Idling")

func shoot() -> void:
	var projectile = PROJECTILE.instance()
	spider.get_parent().add_child(projectile)
	projectile.global_position = spider.raycast_forward.global_position
	projectile.flipped = spider.flipped
	projectile.sender = spider

func _on_AttackTimer_timeout():
	shoot()
