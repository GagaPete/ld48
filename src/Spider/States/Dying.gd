extends SpiderState

func enter() -> void:
	spider.warrior_target.queue_free()
	spider.sprite.play("die")
	yield(spider.sprite, "animation_finished")
	spider.health_bar.visible = false
