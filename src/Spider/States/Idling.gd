extends SpiderState

func enter() -> void:
	spider.sprite.play("idle")

func process(_delta:float) -> void:
	if spider.raycast_forward.is_colliding():
		spider.change_state("Fighting")
