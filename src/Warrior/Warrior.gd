extends Node2D
class_name Warrior

enum Direction { LEFT, RIGHT }

onready var sprite := $AnimatedSprite
onready var raycast_down := $RaycastDown
onready var raycast_forward := $RaycastForward
onready var attack_area := $AttackArea
onready var health_bar := $HealthBar
onready var effects := $Effects

var perk:WarriorPerk = null
var current_state = null
var direction = Direction.RIGHT
var cards_per_chest := 1

func _ready():
	current_state = $States/Walking
	current_state.enter()

func _process(delta):
	current_state.process(delta)

func is_alive():
	return not health_bar.is_empty()

func change_state(state_name:String) -> void:
	var new_state = $States.get_node(state_name)
	if new_state:
		current_state.leave()
		new_state.enter()
		current_state = new_state

func set_perk(new_perk:WarriorPerk) -> void:
	if perk:
		perk.deactivate(self)
		remove_child(perk)
	new_perk.activate(self)
	perk = new_perk
	add_child(perk)
	perk.name = "Perk"
	perk.position = Vector2()

func add_effect(new_effect:WarriorEffect) -> void:
	effects.add_child(new_effect)

func turn_around():
	if direction == Direction.LEFT:
		direction = Direction.RIGHT
	else:
		direction = Direction.LEFT
	update_direction()
	
func update_direction():
	if direction == Direction.LEFT:
		raycast_forward.cast_to.x = -16
		sprite.flip_h = true
		attack_area.position.x = -16
		effects.scale.x = -1
		if perk:
			perk.scale.x = -1
	elif direction == Direction.RIGHT:
		raycast_forward.cast_to.x = 16
		sprite.flip_h = false
		attack_area.position.x = 0
		effects.scale.x = 1
		if perk:
			perk.scale.x = 1

func get_reachable_targets() -> Array:
	var result = []
	for area in attack_area.get_overlapping_areas():
		if not area.has_method("attack"):
			continue
		result.append(area)
	return result

func _on_AttackArea_attacked(damage, attacker):
	if perk:
		damage = perk.filter_incoming_damage(damage, attacker)
	health_bar.active_points -= damage

	if health_bar.is_empty():
		change_state("Dying")
	elif damage > 0 and attacker is Spider:
		change_state("Stunned")
