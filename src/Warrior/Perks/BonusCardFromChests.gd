extends WarriorPerk

func activate(warrior:Warrior) -> void:
	warrior.cards_per_chest += 1

func deactivate(warrior:Warrior) -> void:
	warrior.cards_per_chest -= 1
