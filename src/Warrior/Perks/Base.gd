extends Node2D
class_name WarriorPerk

func activate(warrior) -> void:
	pass

func deactivate(warrior) -> void:
	pass

func filter_incoming_damage(damage:int, _attacker) -> int:
	return damage

func filter_outgoing_damage(damage:int, _target) -> int:
	return damage
