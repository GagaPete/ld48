extends WarriorPerk

onready var animation = $Animation
func filter_incoming_damage(damage:int, attacker) -> int:
	if attacker is Spider:
		animation.play("activate")
		return 0
	return damage
