extends Area2D

signal attacked(damage, attacker)

func attack(damage:float, attacker) -> void:
	emit_signal("attacked", damage, attacker)
