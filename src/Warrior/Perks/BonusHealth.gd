extends WarriorPerk

func activate(warrior) -> void:
	warrior.health_bar.active_points += 2
	warrior.health_bar.bonus_points += 2

func deactivate(warrior) -> void:
	warrior.health_bar.active_points -= 2
	warrior.health_bar.bonus_points -= 2
