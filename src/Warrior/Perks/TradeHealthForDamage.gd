extends WarriorPerk

func activate(warrior:Warrior) -> void:
	warrior.health_bar.regular_points -= 2

func deactivate(warrior:Warrior) -> void:
	warrior.health_bar.regular_points += 2

func filter_outgoing_damage(damage:int, _target) -> int:
	return damage + 2
