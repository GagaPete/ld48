extends WarriorState

export(float) var speed

func enter() -> void:
	.enter()
	warrior.sprite.play("walk")

func process(delta:float) -> void:
	if warrior.direction == Warrior.Direction.LEFT:
		warrior.position.x -= speed * delta
	else:
		warrior.position.x += speed * delta

	if not warrior.raycast_down.is_colliding():
		warrior.change_state("Falling")
	elif warrior.raycast_forward.is_colliding():
		warrior.turn_around()
	elif not warrior.get_reachable_targets().empty():
		warrior.change_state("Fighting")
