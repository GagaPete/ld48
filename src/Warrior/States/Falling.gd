extends WarriorState

export(float) var speed

func enter() -> void:
	.enter()
	warrior.raycast_forward.enabled = false
	warrior.sprite.play("stand")

func leave() -> void:
	warrior.raycast_forward.enabled = true
	.leave()

func process(delta) -> void:
	warrior.position.y += speed * delta
	if warrior.raycast_down.is_colliding():
		warrior.position.y += warrior.raycast_down.get_collision_point().y - warrior.global_position.y
		warrior.change_state("Walking")
