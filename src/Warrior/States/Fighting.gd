extends WarriorState

onready var timer = $AttackTimer

func enter() -> void:
	.enter()
	warrior.sprite.connect("animation_finished", self, "_on_AnimatedSprite_animation_finished")
	attack()

func leave() -> void:
	timer.stop()
	warrior.sprite.disconnect("animation_finished", self, "_on_AnimatedSprite_animation_finished")
	.leave()

func attack() -> void:
	warrior.sprite.frame = 0
	warrior.sprite.play("attack")

func _on_AttackTimer_timeout() -> void:
	attack()

func _on_AnimatedSprite_animation_finished() -> void:
	# Damage all targets in reach
	for target in warrior.get_reachable_targets():
		if target.has_method("attack"):
			var damage = 1
			if warrior.perk:
				damage = warrior.perk.filter_outgoing_damage(damage, target)
			target.attack(damage, warrior)
	
	# Wait a frame to give targets the chance to remove themself
	yield(get_tree(), "idle_frame")
	if active:
		if warrior.get_reachable_targets().empty():
			warrior.change_state("Walking")
		else:
			timer.start()
