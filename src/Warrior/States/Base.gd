extends Node
class_name WarriorState

onready var warrior: Warrior = $"../.."

var active = false

func enter() -> void:
	active = true

func leave() -> void:
	active = false

func process(_delta:float) -> void:
	pass
