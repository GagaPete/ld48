extends WarriorState

func enter() -> void:
	.enter()
	warrior.sprite.play("die")
	# After playing this animation the warrior will remain inactive
