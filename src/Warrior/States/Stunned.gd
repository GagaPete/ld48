extends WarriorState

onready var timer = $RecoverTimer

func enter() -> void:
	.enter()
	warrior.sprite.play("stun")
	timer.start()

func leave() -> void:
	timer.stop()
	.leave()

func _on_RecoverTimer_timeout() -> void:
	warrior.change_state("Walking")
