extends Node

func take_screenshot():
	var file = File.new()
	var data = get_viewport().get_texture().get_data()
	data.flip_y()
	var i = 0
	while file.file_exists("user://screenshot%d.png" % i):
		i += 1
	data.save_png("user://screenshot%d.png" % i)
	print("Screenshot saved as %s/screenshot%d.png" % [OS.get_user_data_dir(), i]);

func _unhandled_input(event):
	if event.is_action_pressed("restart"):
		get_tree().reload_current_scene()

	if event.is_action_pressed("fullscreen"):
		OS.window_fullscreen = not OS.window_fullscreen

	if event.is_action_pressed("screenshot"):
		take_screenshot()

