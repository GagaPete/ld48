tool
extends Node2D
class_name Snake

export(bool) var flipped = false setget set_flipped

onready var sprite = $AnimatedSprite
onready var health_bar = $HealthBar
onready var warrior_target = $WarriorTarget
onready var raycast_forward = $RaycastForward

var current_state

func _ready() -> void:
	if not Engine.editor_hint:
		current_state = $States/Idling
		current_state.enter()

func _process(delta:float) -> void:
	if current_state:
		current_state.process(delta)

func change_state(state_name:String) -> void:
	if not current_state:
		return

	print(state_name)
	current_state.leave()
	var new_state = $States.get_node(state_name)
	if new_state:
		new_state.enter()
	current_state = new_state


func _on_WarriorTarget_attacked(damage:float, _attacker:Warrior) -> void:
	health_bar.active_points -= damage
	if health_bar.active_points <= 0:
		change_state("Dying")

func set_flipped(new_flipped:bool) -> void:
	if not has_node("AnimatedSprite"):
		return
	flipped = new_flipped
	if flipped:
		$AnimatedSprite.scale.x = -1
		$WarriorTarget.position.x = -2
		$RaycastForward.position.x = abs($RaycastForward.position.x)
		$RaycastForward.cast_to.x = abs($RaycastForward.cast_to.x)
	else:
		$AnimatedSprite.scale.x = 1
		$WarriorTarget.position.x = 3
		$RaycastForward.position.x = abs($RaycastForward.position.x) * -1
		$RaycastForward.cast_to.x = abs($RaycastForward.cast_to.x) * -1
