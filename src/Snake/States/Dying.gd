extends SnakeState

func enter() -> void:
	snake.warrior_target.queue_free()
	snake.sprite.play("die")
	yield(snake.sprite, "animation_finished")
	snake.health_bar.visible = false
