extends SnakeState

const PROJECTILE = preload("res://Snake/Projectile.tscn")

onready var timer = $ShootTimer

func enter() -> void:
	.enter()
	timer.start()
	shoot()

func leave() -> void:
	timer.stop()
	.leave()

func process(_delta:float) -> void:
	if not snake.raycast_forward.is_colliding():
		snake.change_state("Idling")

func shoot() -> void:
	var projectile = PROJECTILE.instance()
	snake.get_parent().add_child(projectile)
	projectile.global_position = snake.raycast_forward.global_position
	projectile.flipped = snake.flipped
	projectile.sender = snake
	projectile.travel_distance = snake.raycast_forward.get_collision_point().x - snake.raycast_forward.global_position.x
	
	if snake.raycast_forward.is_colliding():
		timer.start()

func _on_ShootTimer_timeout():
	shoot()
