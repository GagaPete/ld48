extends SnakeState

func enter() -> void:
	snake.sprite.play("idle")

func process(_delta:float) -> void:
	if snake.raycast_forward.is_colliding():
		snake.change_state("Fighting")
