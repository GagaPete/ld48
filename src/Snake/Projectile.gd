tool
extends Node2D
class_name SnakeProjectile

export(bool) var flipped = false setget set_flipped
export(float) var speed

var sender:Snake
var travel_distance:float

func _process(delta):
	var movement:Vector2
	if flipped:
		movement = Vector2(90, 39).normalized() * speed * delta
	else:
		movement = Vector2(-90, 39).normalized() * speed * delta
	position += movement

func _on_Area2D_area_entered(warrior):
	warrior.attack(2, sender)
	$AnimatedSprite.play("splash")
	yield($AnimatedSprite, "animation_finished")
	queue_free()

func set_flipped(new_flipped:bool) -> void:
	flipped = new_flipped
	if not has_node("AnimatedSprite"):
		return
	if flipped:
		scale.x = -1
	else:
		scale.x = 1
