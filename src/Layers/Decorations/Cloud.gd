extends Node2D

export(float) var speed

onready var sprite = $AnimatedSprite
var offset:float

func _ready():
	sprite.frame = rand_range(0, 3)
	offset = position.x

func _process(delta):
	offset += delta * speed
	if offset > (320 + 50):
		offset = -50
		sprite.frame = rand_range(0, 3)
	position.x = floor(offset)
