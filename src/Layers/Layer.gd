tool
extends StaticBody2D

export(int, 0, 3) var background = 0 setget set_background
export(int) var level = 0
export(bool) var flipped = false setget set_flipped

var objects_flipped := false
var contained_warriors := []

func _ready():
	set_background(background)
	set_flipped(flipped)

func set_background(new_background:int) -> void:
	background = new_background
	if not has_node("Background"):
		return
	new_background = min(new_background, $Background.frames.get_frame_count("default"))
	background = new_background
	$Background.frame = new_background

func set_flipped(new_flipped:bool) -> void:
	flipped = new_flipped
	if not has_node("Background"):
		return

	$Background.flip_h = flipped
	if flipped:
		$WallBottom.position.x = 170.5
	else:
		$WallBottom.position.x = 149.5

	if flipped != objects_flipped:
		objects_flipped = flipped

		# Move decorations
		for decoration in $Decorations.get_children():
			decoration.position.x = 180 + (180 - decoration.position.x)

		# Move and flip enemies
		for enemy in $Enemies.get_children():
			enemy.flipped = flipped
			enemy.position.x = 180 + (180 - enemy.position.x)

		# Move treasure chests
		for treasure_chest in $TreasureChests.get_children():
			treasure_chest.position.x = 180 + (180 - treasure_chest.position.x)

func _on_Area2D_area_entered(area):
	contained_warriors.append(area)

func _on_Area2D_area_exited(area):
	contained_warriors.erase(area)
