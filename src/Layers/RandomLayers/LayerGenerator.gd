extends Object

func generate(layer:int):
	var candidates = []
	candidates.append(preload("res://Layers/RandomLayers/EasyLayer0.tscn"))
	candidates.append(preload("res://Layers/RandomLayers/EasyLayer1.tscn"))

	if layer > 4:
		candidates.append(preload("res://Layers/RandomLayers/MiddleLayer0.tscn"))
		candidates.append(preload("res://Layers/RandomLayers/MiddleLayer1.tscn"))
		candidates.append(preload("res://Layers/RandomLayers/MiddleLayer2.tscn"))

	if layer > 9:
		candidates.append(preload("res://Layers/RandomLayers/HardLayer0.tscn"))
		candidates.append(preload("res://Layers/RandomLayers/HardLayer1.tscn"))
		candidates.append(preload("res://Layers/RandomLayers/HardLayer2.tscn"))

	return candidates[rand_range(0, candidates.size())].instance()
