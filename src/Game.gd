extends Node2D

const RESTART_CARD = preload("res://Cards/Menu/Restart.tscn")
const SAVE_PATH = "user://score.dat"

var highscore := 0
var update_layer_display := true

func _ready():
	var file = File.new()
	if file.open(SAVE_PATH, File.READ) == OK:
		highscore = file.get_64()
	file.close()
	
	if highscore > 0:
		$CenterContainer/LayerDisplay.visible = true
		$CenterContainer/LayerDisplay.text = "Deepest Layer - %d" % highscore

func _on_World_progress(layer):
	if layer > 0 and update_layer_display:
		$Sprite/TopFade.visible = true
		$CenterContainer/LayerDisplay.visible = true
		if layer > highscore and highscore > 0:
			$CenterContainer/LayerDisplay.text = "Layer - %d - New Deepest" % layer
		else:
			$CenterContainer/LayerDisplay.text = "Layer - %d" % layer

func _on_World_gameover(level):
	# Handle a highscore okayish
	if level > highscore:
		highscore = level
		save_highscore()
		update_layer_display = false

	$Cards/Stack.visible = false
	for slot in $Cards/Slots.get_children():
		for card in slot.get_children():
			card.queue_free()

	var card = RESTART_CARD.instance()
	$Cards/Slots/Slot1.add_child(card)

func save_highscore():
	var file = File.new()
	if file.open(SAVE_PATH, File.WRITE) == OK:
		file.store_64(highscore)
	file.close()
