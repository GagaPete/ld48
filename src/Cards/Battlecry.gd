extends Card

const PERK = preload("res://Warrior/Perks/BonusDamage.tscn")

func execute() -> void:
	world.set_perk(PERK.instance())
	drop()
