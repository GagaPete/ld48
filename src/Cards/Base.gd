extends Node2D
class_name Card

onready var world = $"/root/Game/Viewport/World"

var dropped := false

func focus() -> void:
	$AnimationPlayer.play("focus")

func unfocus() -> void:
	$AnimationPlayer.play_backwards("focus")

func execute() -> void:
	drop()

func drop() -> void:
	if dropped:
		return
	dropped = true
	$AnimationPlayer.play("drop")
	yield($AnimationPlayer, "animation_finished")
	queue_free()

func _on_ClickArea_input_event(viewport, event, shape_idx):
	if not event is InputEventMouseButton and not event is InputEventScreenTouch:
		return
	if not event.pressed:
		return
	execute()

func _on_ClickArea_mouse_entered():
	if dropped:
		return
	focus()

func _on_ClickArea_mouse_exited():
	if dropped:
		return
	unfocus()
