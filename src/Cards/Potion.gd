extends Card

const PERK = preload("res://Warrior/Perks/BonusHealth.tscn")

func execute():
	world.set_perk(PERK.instance())
	drop()
