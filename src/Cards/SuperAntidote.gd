extends Card

const DEFY_SNAKE_POISON = preload("res://Warrior/Perks/DefySnakePoison.tscn")

func execute() -> void:
	world.set_perk(DEFY_SNAKE_POISON.instance())
	drop()
