extends Card

const PERK = preload("res://Warrior/Perks/BonusCardFromChests.tscn")

func execute() -> void:
	world.set_perk(PERK.instance())
	drop()
