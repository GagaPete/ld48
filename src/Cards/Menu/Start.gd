extends Card

var is_executed := false

func execute():
	if is_executed:
		return
	is_executed = true

	world.spawn_warrior()
	for slot in $"../../".get_children():
		for card in slot.get_children():
			card.drop()
