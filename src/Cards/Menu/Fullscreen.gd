extends Card

func execute():
	OS.window_fullscreen = not OS.window_fullscreen
	_update_animation()

func _ready():
	if OS.has_feature("web"):
		queue_free()
	_update_animation()

func _update_animation():
	if OS.window_fullscreen:
		$AnimatedSprite.play("minimize")
	else:
		$AnimatedSprite.play("maximize")
