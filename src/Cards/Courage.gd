extends Card

const PERK = preload("res://Warrior/Perks/TradeHealthForDamage.tscn")

func execute() -> void:
	world.set_perk(PERK.instance())
	drop()
