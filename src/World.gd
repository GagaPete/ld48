extends Node2D

signal progress(level)
signal gameover(level)

const WARRIOR = preload("res://Warrior/Warrior.tscn")
const BANDAGE = preload("res://Warrior/Effects/Bandage.tscn")
const HOLY_BANDAGE = preload("res://Warrior/Effects/HolyBandage.tscn")

onready var current_spawn_level = $Layers.get_child_count()
onready var camera = $Camera
onready var layers = $Layers
onready var warriors = $Warriors

var LayerGenerator = preload("res://Layers/RandomLayers/LayerGenerator.gd").new()
var current_camera_level := 0
var gameon := false

func spawn_warrior() -> void:
	var warrior = WARRIOR.instance()
	warriors.add_child(warrior)
	warrior.global_position = warriors.global_position
	gameon = true
	
func add_effect(effect_name:String) -> void:
	pass

func set_perk(perk:WarriorPerk) -> void:
	for warrior in warriors.get_children():
		warrior.set_perk(perk)

func heal(points:int, holy:bool) -> bool:
	var warrior_list = Array()
	for warrior in warriors.get_children():
		if not warrior.health_bar.is_empty() and not warrior.health_bar.is_full():
			warrior_list.append(warrior)
	if warrior_list.empty():
		return false

	var health_per_warrior = max(floor(points / warrior_list.size()), 1)
	var extra_health_count = fmod(points, warrior_list.size())
	for warrior in warrior_list:
		var health_boost = health_per_warrior
		if extra_health_count > 0:
			health_boost += 1
			extra_health_count -= 1
		warrior.health_bar.active_points += health_boost
		if holy:
			warrior.add_effect(HOLY_BANDAGE.instance())
		else:
			warrior.add_effect(BANDAGE.instance())
	return true

func _ready():
	randomize()

func _process(_delta):
	var layer_count = layers.get_child_count()

	# Pushes a new layer
	var last_layer = layers.get_child(layers.get_child_count() - 1)
	if not last_layer.contained_warriors.empty():
		_push_layer()

	# Pop all unused layers
	for layer in layers.get_children():
		if layer_count == 3 or not layer.contained_warriors.empty():
			break
		_shift_layer()
		layer_count -= 1
	
	# Move camera to the latest used layer
	last_layer = -1
	for layer in layers.get_children():
		if not layer.contained_warriors.empty():
			last_layer = layer.level
	if last_layer == 0:
		camera.global_position.y = 95
	elif last_layer > 0:
		camera.global_position.y = 100 + 120 * last_layer
		current_camera_level = last_layer
	
	# Inform those who care about out progress
	emit_signal("progress", last_layer)

	# Check for gameover after the game has started
	if gameon:
		for warrior in warriors.get_children():
			if warrior.is_alive():
				return
		emit_signal("gameover", last_layer)

func _push_layer() -> void:
	var layer = LayerGenerator.generate(current_spawn_level)
	layer.name = "Layer" + str(current_spawn_level)
	layer.flipped = (current_spawn_level % 2) == 1
	layers.add_child(layer)
	layer.global_position = Vector2(0, 60 + 120 * current_spawn_level)
	layer.level = current_spawn_level
	current_spawn_level += 1

func _shift_layer() -> void:
	$Layers.get_child(0).queue_free()
