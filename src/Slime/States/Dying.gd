extends SlimeState

func enter() -> void:
	slime.warrior_target.queue_free()
	slime.sprite.play("die")
	yield(slime.sprite, "animation_finished")
	slime.queue_free()
