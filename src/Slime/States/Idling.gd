extends SlimeState

onready var jump_timer := $JumpTimer

func enter() -> void:
	slime.sprite.play("idle")
	jump_timer.start(rand_range(0.5, 1.5))

func leave() -> void:
	jump_timer.stop()

func process(_delta:float) -> void:
	if slime.raycast_forward.is_colliding():
		slime.change_state("Fighting")

func _on_JumpTimer_timeout() -> void:
	slime.sprite.frame = 0
	jump_timer.start(rand_range(0.0, 1.5))
