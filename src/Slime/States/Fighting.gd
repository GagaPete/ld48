extends SlimeState

onready var timer := $AttackTimer

func enter() -> void:
	.enter()
	slime.sprite.connect("animation_finished", self, "_on_AnimationSprite_animation_finished")
	attack()

func leave() -> void:
	timer.stop()
	slime.sprite.disconnect("animation_finished", self, "_on_AnimationSprite_animation_finished")
	.leave()

func attack() -> void:
	slime.sprite.offset.x = -5
	slime.sprite.frame = 0
	slime.sprite.play("attack")

func _on_AttackTimer_timeout() -> void:
	attack()

func _on_AnimationSprite_animation_finished() -> void:
	# Damage the warrior
	var warrior = slime.raycast_forward.get_collider()
	if warrior:
		warrior.attack(1, slime)

	if slime.raycast_forward.is_colliding():
			timer.start()
	else:
		warrior.change_state("Idling")
