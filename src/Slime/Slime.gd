tool
extends Node2D
class_name Slime

const REGULAR_SKIN = preload("res://Slime/RegularSkin.tres")
const STRONG_SKIN = preload("res://Slime/StrongSkin.tres")

export(bool) var flipped = false setget set_flipped
export(bool) var strong = false setget set_strong

onready var raycast_forward := $RaycastForward
onready var sprite := $AnimatedSprite
onready var warrior_target := $WarriorTarget
onready var health_bar := $HealthBar

var current_state = null

func _ready():
	if not Engine.editor_hint:
		current_state = $States/Idling
		current_state.enter()

func _process(delta):
	if current_state:
		current_state.process(delta)

func change_state(state_name:String) -> void:
	if not current_state:
		return

	current_state.leave()
	var new_state = $States.get_node(state_name)
	if new_state:
		new_state.enter()
	current_state = new_state

func _on_AttackTarget_attacked(damage:float, _attacker:Warrior) -> void:
	health_bar.active_points -= damage
	if health_bar.active_points <= 0:
		change_state("Dying")

func set_flipped(new_flipped:bool) -> void:
	if not has_node("AnimatedSprite"):
		return
	flipped = new_flipped
	if flipped:
		$AnimatedSprite.scale.x = -1
		$RaycastForward.cast_to.x = 30
	else:
		$AnimatedSprite.scale.x = 1
		$RaycastForward.cast_to.x = -30

func set_strong(new_strong:bool) -> void:
	if not has_node("AnimatedSprite"):
		return
	strong = new_strong
	if strong:
		$AnimatedSprite.frames = STRONG_SKIN
		$HealthBar.regular_points = 5
		$HealthBar.active_points = 5
	else:
		$AnimatedSprite.frames = REGULAR_SKIN
		$HealthBar.regular_points = 2
		$HealthBar.active_points = 2
