extends Node2D

export(int, FLAGS,
	"Antidote",
	"Bandage",
	"Battlecry",
	"BigShield",
	"Courage",
	"Focus",
	"Haste",
	"HolyBandage",
	"Knife",
	"LastChance",
	"Potion",
	"Scissors",
	"Shield",
	"SuperAntidote",
	"Swordsmaster",
	"Thunderstorm",
	"TreasureLuck"
) var available_cards = 0xffffffff

const CARD_SCENES = {
	#0: preload("res://Cards/Antidote.tscn"),
	1: preload("res://Cards/Bandage.tscn"),
	2: preload("res://Cards/Battlecry.tscn"),
	#3: preload("res://Cards/BigShield.tscn"),
	4: preload("res://Cards/Courage.tscn"),
	#5: preload("res://Cards/Focus.tscn"),
	#6: preload("res://Cards/Haste.tscn"),
	7: preload("res://Cards/HolyBandage.tscn"),
	#8: preload("res://Cards/Knife.tscn"),
	#9: preload("res://Cards/LastChance.tscn"),
	10: preload("res://Cards/Potion.tscn"),
	11: preload("res://Cards/Scissors.tscn"),
	#12: preload("res://Cards/Shield.tscn"),
	13: preload("res://Cards/SuperAntidote.tscn"),
	#14: preload("res://Cards/Swordsmaster.tscn"),
	#15: preload("res://Cards/Thunderstorm.tscn"),
	16: preload("res://Cards/TreasureLuck.tscn"),
}

const CARD_WEIGHTS = {
	1: 100,
	2: 50,
	4: 10,
	7: 10,
	10: 50,
	11: 80,
	13: 80,
	16: 50,
}

onready var sprite := $AnimatedSprite

var is_open := false

func open(attacker:Warrior) -> void:
	if is_open:
		return

	is_open = true
	sprite.play("opening")
	$WarriorTarget.queue_free()
	
	yield(sprite, "animation_finished")

	var idx = 0
	var valid_card_scenes = {}
	var valid_card_weights = {}
	var total_weight = 0

	for id in CARD_SCENES.keys():
		if available_cards & (1 << id):
			valid_card_scenes[idx] = CARD_SCENES[id]
			valid_card_weights[idx] = CARD_WEIGHTS[id]
			total_weight += CARD_WEIGHTS[id]
			idx += 1

	for _i in range(0, attacker.cards_per_chest):
		var choice = rand_range(0, total_weight)
		for idx2 in valid_card_scenes.keys():
			choice -= valid_card_weights[idx2]
			if choice < 0:
				var card = valid_card_scenes[idx2].instance()
				$"/root/Game/Cards".add_card(card)
				break

func _on_WarriorTarget_attacked(_damage:float, attacker:Warrior):
	open(attacker)
